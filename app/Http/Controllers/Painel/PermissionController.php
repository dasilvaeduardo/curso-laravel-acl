<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Permission;

class PermissionController extends Controller
{
    protected $permission;
    protected $totalPage = 10;

    public function __construct(Permission $permission){
        $this->permission = $permission;
    }
    
    
    public function index(){
        $permissions = $this->permission->paginate($this->totalPage);

        return view('painel.permission.index', compact('permissions'));
    }

    public function roles($id){

        $permission = $this->permission->find($id);
        
        $roles = $permission->roles()->get();

        return view('painel.permission.roles', compact('permission', 'roles'));
    }
}
