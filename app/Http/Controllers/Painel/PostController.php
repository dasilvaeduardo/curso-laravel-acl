<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;

class PostController extends Controller
{
    protected $post;
    protected $totalPage = 10;

    public function __construct(Post $post){
        $this->post = $post;
    }
    
    
    public function index(){
        $posts = $this->post->paginate($this->totalPage);

        return view('painel.post.index', compact('posts'));
    }
}
