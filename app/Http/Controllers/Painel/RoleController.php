<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Role;

class RoleController extends Controller
{
    protected $role;
    protected $totalPage = 10;

    public function __construct(Role $role){
        $this->role = $role;
    }
    
    
    public function index(){
        $roles = $this->role->paginate($this->totalPage);

        return view('painel.roles.index', compact('roles'));
    }

    public function permissions($id){

        $role = $this->role->find($id);
        
        $permissions = $role->permissions()->get();

        return view('painel.roles.permissions', compact('role','permissions'));
    }
    
}
