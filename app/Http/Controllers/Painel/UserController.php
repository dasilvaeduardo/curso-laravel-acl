<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    protected $user;
    protected $totalPage = 10;

    public function __construct(User $user){
        $this->user = $user;
    }
    
    
    public function index(){
        $users = $this->user->paginate($this->totalPage);
        
        return view('painel.user.index', compact('users'));
    }

    public function roles($id){

        $user = $this->user->find($id);
        
        $roles = $user->roles()->get();

        return view('painel.user.roles', compact('user', 'roles'));
    }
}
