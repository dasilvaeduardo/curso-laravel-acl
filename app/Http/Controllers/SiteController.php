<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Post;
use Gate;

class SiteController extends Controller
{
    protected $post;
    
    public function __construct(Post $post)
    {
        $this->post = $post;
        //$this->middleware('auth');
    }

    public function index()
    {
        $posts = $this->post->all();

        return view('site.index', compact('posts'));
    }

    /*
    public function update($id)
    {
        $post = $this->post->find($id);

        //Definição desta autorização está em APP\Providers\AuthServiceProvider.php
        $this->authorize('update-post', $post);

        return view('painel.post.update', compact('post'));
    }


    public function rolesPermissions(){
        echo "Nome: ". auth()->user()->name;
        echo "<BR><hr>";

        foreach (auth()->user()->roles as $role) {
            echo "Perfil: ". $role->name;
            echo "<br>";

            $permissions = $role->permissions;

            echo "Permissões: ";

            foreach ($permissions as $permission) {
                echo $permission->name;
                echo ", ";    
            }

            echo "<BR><hr>";

        }
    }
    */

}
