<?php
/*
Route::get('/home', 'HomeController@index');

Route::get('/post/{id}/update', 'HomeController@update');

Route::get('/roles-permissions', 'HomeController@rolesPermissions');
*/

Route::group(['prefix' => 'painel', 'namespace' => 'Painel'], function(){
    
    //Gestão do Painel
    Route::get('/', 'PainelController@index');

    //Gestão dos Posts
    Route::get('/posts', 'PostController@index');

    //Gestão das Permissions
    Route::get('/permissions', 'PermissionController@index');
    Route::get('/permissions/{id}/roles', 'PermissionController@roles');

    //Gestão dos Roles
    Route::get('/roles', 'RoleController@index');
    Route::get('/roles/{id}/permissions', 'RoleController@permissions');
    
    //Gestão dos Usuários
    Route::get('/users', 'UserController@index');
    Route::get('/users/{id}/roles', 'UserController@roles');
});

Route::auth();

Route::get('/', 'SiteController@index');
