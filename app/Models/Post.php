<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    //Relacionamento N-1
    public function user(){
        return $this->belongsTo(User::class);
    }
}
