<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class Role extends Model
{
    //Retorna todas as permissões 
    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
}
