<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Post;
use App\Models\Permission;
use App\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Models\Post' => 'App\Policies\PostPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        
        $this->registerPolicies($gate);
        /*
        $gate->define('update-post', function(User $user, Post $post){
            return $user->id == $post->user_id;
        });
        */

        //Recuperando todas as permissões e trazendo os objetos de funções destas permissões (Roles)
        //Ex.:  view_post => Manager, Adm, Edit
        //      delete_post => Adm, Manager
        $permissions = Permission::with('roles')->get();

        //Envia cada permissão resgatada na variável $permissions para  a função hasPermission da Model User
        foreach ($permissions as $permission) {
            $gate->define($permission->name, function(User $user) use ($permission){
                return $user->hasPermission($permission);
            });
        }

    }
}
