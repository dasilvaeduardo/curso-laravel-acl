<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Permission;
use App\Models\Role;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Método para trazer todas as funções que o user tem
    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    //De acordo com a permissão recuperada, envia a função (Role) para a função hasAnyRoles
    public function hasPermission(Permission $permission)
    {
        return $this->hasAnyRoles($permission->roles);
    }

    //Verifica se o usuário logado tem a função enviada
    public function hasAnyRoles($roles)
    {
        if(is_array($roles) || is_object($roles)){
            return !! $roles->intersect($this->roles)->count();
        }
        
        return $this->roles->contains('name', $roles);
        
    }

}
