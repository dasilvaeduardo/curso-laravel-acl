@extends('auth.template.index')

@section('content')
<form class="login form" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" placeholder="Digite seu e-mail" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input id="password" type="password" class="form-control" placeholder="Digite sua senha"  name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12 col-md-offset-4">
                    <button type="submit" class="btn btn-login">
                        <i class="fa fa-btn fa-sign-in"></i> Entrar
                    </button>
                </div>
            </div>

            <div class="form-group text-right">
                <a class="recuperar" href="{{ url('/password/reset') }}">Recuperar senha?</a>
            </div>
        </form>
@endsection