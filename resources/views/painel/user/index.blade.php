@extends('painel.template.index')

@section('content')

<div class="clear"></div>

<!--Filters and actions-->
<div class="actions">
    <div class="container">
        <a class="add" href="forms">
            <i class="fa fa-plus-circle"></i>
        </a>

        <form class="form-search form form-inline">
            <input type="text" name="pesquisar" placeholder="Pesquisar?" class="form-control">
            <input type="submit" name="pesquisar" value="Encontrar" class="btn btn-success">
        </form>
    </div>
</div><!--Actions-->

<div class="container">
	<h1 class="title">
		Listagem dos Usuários
	</h1>

	<table class="table table-hover">
	  <tr>
	  	<th>#</th>
	  	<th>Nome</th>
	  	<th>E-mail</th>
	  	<th width="150px">Ações</th>
	  </tr>

        @forelse ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <a href="{{ url("painel/users/$user->id/roles") }}" class="visual">
                        <i class="fa fa-lock"></i>
                    </a>
                    <a href="{{ url("painel/users/$user->id/edit") }}" class="edit">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>
                    <a href="{{ url("painel/users/$user->id/delete") }}" class="delete">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="90">Nenhum usuário cadastrado!</td>
            </tr>

        @endforelse
	  
	</table>
    
</div>

@endsection