
<a href="/login">Login</a>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @forelse ($posts as $post)
                        <ul class="list-unstyled">
                            <li class="media">
                            <div class="media-body">
                                <h5 class="mt-0 mb-1"> {{ $post->title }} </h5>
                                <p> {{$post->user->name}} </p>
                                {{ $post->description }}
                                <br>
                                @can('edit-post', $post) 
                                    <a href="{{ url("/post/$post->id/update") }}"> Editar  </a>
                                @endcan
                            </div>
                            </li>
                        </ul>                        
                    @empty
                    <p>Nenhum post cadastrado!!!</p>                        
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
